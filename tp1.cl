;; Ex 1
;; 4)
(defun nombres3 (xx)               
                (
                 if (and 
                      (numberp(car xx))
                      (numberp(cadr xx))
                      (numberp(caddr xx))
                     )
                 'bravo
                 'perdu
               )
  )

(defun grouper (l1 l2)
               (
                if (and (not (equal l1 ())) (not (equal l2 ())))
                   (cons (list (car l1) (car l2)) (grouper (cdr l1) (cdr l2)))
               )
              )

(defun monReverse (l1) 
  (
    if (null l1)
    ()
    (append (monReverse (cdr l1)) (list (car l1)))
  )
)

(defun palindrome (l1)
  (equal l1 (monReverse l1)))
)

(defun monEqual (l1 l2)
  (
    if (and (list l1) (list 2))
      (equal l1 l2)
      (eq l1 l2)
  )
)

;; Ex 2
(defun list-triple-couple (l1)
  (
    if (null l1)
    ()
    (append (
              list (list (car l1) ((lambda (x) (* x 3))(car l1)))
            ) (list-triple-couple (cdr l1))
    )
  )
)

;; Ex 3
(defun my-assoc (cle list)
  (if (null list)
    nil
    (
      if (not (null (member cle (car list))))
        (car list)
        (my-assoc cle (cdr list))
    )
  )
)

(defun cles (list)
  ( if (null list)
    ()
    (append (list (caar list) ) (cles (cdr list)))
  )
)

(defun creation (l1 l2)
  (if (or (null l1) (null l2))
    ()
    (
      cons (list (car l1) (car l2) ) (creation (cdr l1) (cdr l2))
    )
  )
)

;; Ex 4
(setq BaseTest 
  '(
      ("Campagnes de Clovis Ier" 486 508 ( ("Royaume Franc") ("Domaine gallo-romain de Soissons" "Alaman" "Burgondes" "Wisigoth" "Ostrogoth") ("Soissons" "Zulpich" "Dijon" "Vouille" "Vienne" "Arles" "Bouches-du-Rhone") ) )
      ("Guerre de Burgondie" 523 533 ( ("Royaume Franc") ("Burgondes") ) ("Vezeronce" "Arles" ) )
      ("Conquete de la Thuringe" 531 531 ( ("Royaume Franc") ("Thuringes") ) ("Thuringe"))
      ("Guerre des Goths" 535 553 ( ("Royaume ostrogoth" "Alamans Royaume franc" "Royaume wisigoth Burgondes") ("Empire Byzantin")) ("Peninsule italienne"))
      ("Conquete de la Baviere" 555 555 (("Royaume franc") ("Bavarii")) ("Baviere"))
      ("Campagnes de Bretagne" 560 578 (("Royaume franc	") ("Royaume du Vannetais")) ("Vannetais"))
      ("Guerre de succession merovingienne" 584 585 (("Royaume d'Aquitaine")("Royaume d'Aquitaine"))("Comminges"))
      ("Guerre franco-frisonne" 600 793 (("Royaume franc")("Royaume de Frise"))("Pays-Bas" "Allemagne"))
      ("Guerre civile des Francs" 715 719 (("Neustrie")("Austrasie"))("Royaume franc"))
      ("Invasion omeyyade en France" 719 759 (("Royaume franc")("Califat omeyyade"))("Royaume d'Aquitaine" "Septimanie"))
      ("Guerre des Lombards" 755 758 (("Royaume franc") ("Lombards")) ("Lombardie"))
      ("Guerre d'Aquitaine" 761 768 (("Royaume franc") ("Aquitains"))("Vasconie Aquitaine"))
      ("Guerre des Saxons" 772 804 (("Royaume franc") ("Saxons"))("Germanie"))
      ("Guerre des Lombards" 773 774 (("Royaume franc")("Lombards"))("Lombardie"))
      ("Guerre des Avars" 791 805 (("Royaume de France") ("Avars"))("Pannonie"))
      ("Invasions sarrasines en Provence" 798 990 (("Royaume de France" "Comté de Provence")("Sarrasins"))("Provence"))
      ("Luttes interdynastiques carolingiennes" 830 842 (("Francie occidentale" " Francie orientale")("Francie mediane"))("Fontenoy"))
      ("Guerre franco-bretonne" 843 851 (("Royaume de France")("Royaume de Bretagne" "Vikings"))("Royaume de Bretagne"))
      ("Luttes inter-dynastiques carolingiennes" 876 946 (("Francie occidentale" "Francie orientale")("Royaume de Bourgogne" "Francie orientale"))("Ardennes" "Saône-et-Loire" "Rhénanie-Palatinat" "Aisne"))
      ("Invasions vikings en France" 799 1014 (("Royaume de France")("Vikings"))("Normandie" "Bretagne"))
  )
)

(defun dateDebut (conflit) (cadr conflit))
(defun nomConflit (conflit) (car conflit))
(defun allies (conflit) (car (cadddr conflit)))
(defun ennemis (conflit) (cadr (cadddr conflit)))
(defun lieu (conflit) (car (last conflit)))

(defun affiche-conflits (conflits)
  (cond
      ((null conflits) 'fin)
      ( 
        t 
        (print (car conflits))
        (affiche-conflits (cdr conflits) )
      )
  ) 
)

(defun affiche-allies (conflits allie)
  (cond
    ((null conflits) 'fin)
    (t
      (let* 
        ( 
          (conflit (car conflits))
          (ListAllies (allies conflit))
        )
        (if (member allie ListAllies :test #'string=)
          (print conflit)
        )
      )
      (affiche-allies (cdr conflits) allie)
    )  
  )
)

(defun affiche-dateDebut(conflits from to)
  (cond
    ((null conflits) 'fin)
    (t
      (let* 
        ( 
          (conflit (car conflits))
          (date (dateDebut conflit))
        )
        (if (and (>= date from) (<= date to) )
          (print conflit)
        )
      )
      (affiche-dateDebut (cdr conflits) from to)
    )  
  )
)

(defun nombre-conflits-ennemi(conflits ennemi)
  (cond
    ((null conflits) '0)
    (t
      (let* 
        ( 
          (conflit (car conflits))
          (ListEnnemis (ennemis conflit))
        )
        (if (member ennemi ListEnnemis :test #'string=)
          (+ 1 (nombre-conflits-ennemi (cdr conflits) ennemi))
          (nombre-conflits-ennemi (cdr conflits) ennemi)
        )
      )
    )  
  )
)

(defun fb1() (affiche-conflits BaseTest))
(defun fb2() (affiche-allies BaseTest "Royaume Franc"))
(defun fb3(allie) (affiche-allies BaseTest allie))
(defun fb4()(affiche-dateDebut BaseTest 523 2019))
(defun fb5()(affiche-dateDebut BaseTest 523 715))
(defun fb6() (nombre-conflits-ennemi BaseTest "Lombards"))